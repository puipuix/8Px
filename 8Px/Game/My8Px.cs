using Godot;
using GDExtension;
using Px.Game;
using System;
using System.Linq;

public class My8Px : Sprite
{
	enum GameMode { Demo, Running, Score, Pause }

	private Image _img = new Image();
	private MyGrid _grid = new MyGrid();

	private float _difficulty;
	private float _healTimeout = -1;
	private float _pieceValueTimeout = -1;

	private GameMode _gameMode = GameMode.Demo;
	private int _pieceI, _pieceJ;
	private MyPiece _piece;
	private int _pieceValue;

	private int _pauseWave;
	private float _pauseWaveTimeout = 0.1f;

	private int _score = 0;
	private int _scoreToPrint = 0;

	[GDEReady("ActionAudio")]
	private AudioStreamPlayer _actionAudio = null;

	[GDEReady("ErrorAudio")]
	private AudioStreamPlayer _errorAudio = null;
	
	[GDEReady("PlacementAudio")]
	private AudioStreamPlayer _placementAudio = null;

	public override void _Ready()
	{
		base._Ready();
		GDEReadyAttribute.SetUp(this);
		_img.Create(8, 8, false, Image.Format.Rgbf);
		Start();
	}

	private void Start()
	{
		_difficulty = -1f;
		_healTimeout = 30;
		_grid.Clear();
		_gameMode = GameMode.Running;
		_piece = EMyPiece.GetRandom();
		NewPiece(_piece);
	}

	private void GameOver()
	{
		_gameMode = GameMode.Score;
		_scoreToPrint = 0;
		_score = (int)_difficulty * 20;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				_score += (MyGrid.MaxValue - _grid[i, j]) / 2;
			}
		}
	}

	private Color ValueToColor(int value)
	{
		float r = Mathf.Clamp(value, 0, 255);
		float g = Mathf.Clamp(value - 255, 0, 255);
		float b = Mathf.Clamp(value - 255 - 255, 0, 255);
		return new Color(r / 255, g / 255, b / 255);
	}

	private void DrawGrid()
	{
		_img.Lock();
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				_img.SetPixel(i, j, ValueToColor(_grid[i, j]));
			}
		}
		_piece.Draw(_pieceI, _pieceJ, _grid, _pieceValue, _img);
		_img.Unlock();
		(Texture as ImageTexture).CreateFromImage(_img);
	}

	private void DrawPause(float delta)
	{
		_pauseWaveTimeout -= delta;
		if (_pauseWaveTimeout < 0)
		{
			_pauseWaveTimeout = 0.1f;
			_pauseWave++;
		}
		_img.Fill(Colors.Black);
		_img.Lock();
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				if (i == 1 || i == 2 || i == 5 || i == 6)
				{
					_img.SetPixel(i, j, Colors.White);
				}
				else
				{
					_img.SetPixel(i, j, ValueToColor((int)((Math.Cos((15 - i - j + _pauseWave) * 0.5) + Math.PI) / Math.PI * 0.5 * MyGrid.MaxValue) % MyGrid.MaxValue));
				}
			}
		}
		_img.Unlock();
		(Texture as ImageTexture).CreateFromImage(_img);
	}

	private void DrawScore(int score)
	{
		_img.Fill(Colors.Black);
		_img.Lock();
		for (int j = 0; j < 8 && score > 0; j++)
		{
			for (int i = 0; i < 8 && score > 0; i++)
			{
				int value;
				if (score > MyGrid.MaxValue)
				{
					value = MyGrid.MaxValue;
					score -= MyGrid.MaxValue;
				}
				else
				{
					value = score;
					score = 0;
				}
				_img.SetPixel(i, j, ValueToColor(value));
			}
		}
		_img.Unlock();
		(Texture as ImageTexture).CreateFromImage(_img);
	}


	private void AutoHeal()
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				if (_grid.IsAlive(i, j))
				{
					_grid[i, j] += 5;
				}
			}
		}
	}

	private void NewPiece(MyPiece p)
	{
		var old = _piece;
		_piece = p;
		_piece.EnterGrid(ref _pieceI, ref _pieceJ, old);
		_pieceValue = 100;
		_pieceValueTimeout = 1;
		_difficulty += 1;
	}

	private bool TryPlacePiece()
	{
		if (_piece.CanPlaceOn(_pieceI, _pieceJ, _grid))
		{
			_piece.PlaceOn(_pieceI, _pieceJ, _grid, _pieceValue);
			NewPiece(EMyPiece.GetRandom());
			return true;
		}
		else
		{
			return false;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (_gameMode == GameMode.Demo)
		{
			_gameMode = GameMode.Running;
		}
		else if (_gameMode == GameMode.Pause)
		{
			if (Input.IsActionJustPressed("ui_cancel"))
			{
				_gameMode = GameMode.Running;
			}
			DrawPause(delta);
		}
		else if (_gameMode == GameMode.Running)
		{
			if (Input.IsActionJustPressed("ui_cancel"))
			{
				_gameMode = GameMode.Pause;
				_pauseWave = 0;
			}
			else
			{
				bool input = false;
				if (Input.IsActionJustPressed("ui_accept"))
				{
					if (TryPlacePiece())
					{
						_placementAudio.Play();
					}
					else
					{
						_errorAudio.Play();
					}
				}

				if (Input.IsActionJustPressed("ui_select"))
				{
					input = true;
					var old = _piece;
					_piece = _piece.Rotated;
					_piece.EnterGrid(ref _pieceI, ref _pieceJ, old);
					_actionAudio.Play();
				}

				if (Input.IsActionJustPressed("ui_left"))
				{
					if (_piece.CanMoveTo(_pieceI - 1, _pieceJ))
					{
						input = true;
						_pieceI--;
						_actionAudio.Play();
					}
					else
					{
						_errorAudio.Play();
					}
				}

				if (Input.IsActionJustPressed("ui_right"))
				{
					if (_piece.CanMoveTo(_pieceI + 1, _pieceJ))
					{
						input = true;
						_pieceI++;
						_actionAudio.Play();
					}
					else
					{
						_errorAudio.Play();
					}
				}

				if (Input.IsActionJustPressed("ui_up"))
				{
					if (_piece.CanMoveTo(_pieceI, _pieceJ - 1))
					{
						input = true;
						_pieceJ--;
						_actionAudio.Play();
					}
					else
					{
						_errorAudio.Play();
					}
				}

				if (Input.IsActionJustPressed("ui_down"))
				{
					if (_piece.CanMoveTo(_pieceI, _pieceJ + 1))
					{
						input = true;
						_pieceJ++;
						_actionAudio.Play();
					} else
					{
						_errorAudio.Play();
					}
				}

				if (!input)
				{
					_difficulty += delta;
					_healTimeout -= delta;
					_pieceValueTimeout -= delta;

					if (_pieceValueTimeout < 0)
					{
						_pieceValueTimeout = Math.Max(0.1f, 1 - _difficulty * 0.01f);
						_pieceValue += 10;
						if (_pieceValue > MyGrid.MaxValue / 2)
						{
							if (TryPlacePiece())
							{
								_placementAudio.Play();
							}
							else
							{
								GameOver();
								_errorAudio.Play();
							}
						}
					}

					if (_healTimeout < 0)
					{
						_healTimeout = 1 + _difficulty * 0.01f;
						AutoHeal();
					}
				}
			}

			DrawGrid();
		}
		else if (_gameMode == GameMode.Score)
		{
			_scoreToPrint = Math.Min(_scoreToPrint + (int)Math.Max(1, _score * 0.2f * delta), _score);

			if (Input.IsActionJustPressed("ui_accept"))
			{
				if (_scoreToPrint > _difficulty - 1)
				{
					Start();
				}
			}

			DrawScore(_scoreToPrint);
		}
	}
}
