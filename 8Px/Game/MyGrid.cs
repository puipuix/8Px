﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Px.Game
{
    public class MyGrid
    {

        public const int MaxValue = 765;
        private int[][] _values = Enumerable.Range(0, 8).Select(i => Enumerable.Range(0, 8).Select(j => MaxValue).ToArray()).ToArray();

        public int this[int i, int j]
        {
            get {
                try
                {
                    return _values[i][j];
                }
                catch (IndexOutOfRangeException e)
                {
                    throw new IndexOutOfRangeException("[" + i + ", " + j + "]", e);
                }
            }
            set {
                try
                {
                    _values[i][j] = Mathf.Clamp(value, 0, MaxValue);
                }
                catch (IndexOutOfRangeException e)
                {
                    throw new IndexOutOfRangeException("[" + i + ", " + j + "]", e);
                }
            }
        }

        public void Clear()
        {
            _values = Enumerable.Range(0, 8).Select(i => Enumerable.Range(0, 8).Select(j => MaxValue).ToArray()).ToArray();
        }

        public bool IsAlive(int i, int j)
        {
            return this[i, j] > 0;
        }

        public bool WillBeAlive(int i, int j, int value)
        {
            return this[i, j] - value > 0;
        }
    }
}
