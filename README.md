# 8Pixel
8Pixel is a small puzzle game where the goal is to put pieces of different shapes on a grid, being careful not to destroy a square by overloading it. The different squares take shades of white, yellow, orange, red and finally black according to their life. When placing a piece, a square is drawn in green if placing the piece will not destroy the square, in cyan if the square will be destroyed and in blue if it is not possible to place the piece.

**Controls**

 - Directional arrows for moving a piece
 - Space to turn a piece
 - Enter to put a piece
 - Escape to pause or resume
